﻿using SharedLibrary;
using System;
using System.Linq;
using System.Numerics;

namespace Day19
{
    class Program
    {
        public enum Direction
        {
            Up,
            Down,
            Right,
            Left
        }

        static void Main(string[] args)
        {
            var input = FileHelper.GetLines("Day19").ToList();
            var grid = new char[input[0].Length, input.Count];

            var coordinates = Vector2.Zero;
            for (var y = 0; y < input.Count; y++)
            {
                var line = input[y];
                for (var x = 0; x < line.Length; x++)
                {
                    var letter = line[x];
                    grid[x, y] = letter;
                    if (y == 0 && letter != ' ')
                    {
                        coordinates = new Vector2(x, y);
                    }
                }
            }

            var direction = Direction.Down;
            var path = "";
            var steps = 0;
            while (true)
            {
                if (coordinates.X == -1 || coordinates.Y == -1) break;
                steps++;
                var nodeValue = grid[(int)coordinates.X, (int)coordinates.Y];
                switch (nodeValue)
                {
                    case '|':
                    case '-':
                        Advance();
                        break;
                    case '+':
                        coordinates = FindNextNode();
                        break;
                    case ' ':
                        coordinates.X = -1;
                        break;
                    default:
                        path += nodeValue.ToString();
                        Advance();
                        break;
                }
            }

            Console.WriteLine(path);
            Console.WriteLine(steps - 1);
            Console.ReadKey();

            Vector2 FindNextNode()
            {
                var xStart = -1;
                var yStart = -1;
                var yMax = 1;
                var xMax = 1;

                switch (direction)
                {
                    case Direction.Up:
                        yStart = 1;
                        break;
                    case Direction.Down:
                        yMax = -1;
                        break;
                    case Direction.Right:
                        xStart = 1;
                        break;
                    case Direction.Left:
                        xMax = -1;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                for (var x = xStart; x <= xMax; x += 2)
                {
                    var newCoordinates = new Vector2(coordinates.X + x, coordinates.Y);
                    if (newCoordinates.X < 0 || newCoordinates.X >= grid.GetLength(0)) continue;

                    var newValue = grid[(int)newCoordinates.X, (int)newCoordinates.Y];
                    if (newValue != ' ')
                    {
                        direction = x == -1 ? Direction.Left : Direction.Right;
                        return newCoordinates;
                    }
                }

                for (var y = yStart; y <= yMax; y += 2)
                {
                    var newCoordinates = new Vector2(coordinates.X, coordinates.Y + y);
                    if (newCoordinates.Y < 0 || newCoordinates.Y >= grid.GetLength(1)) continue;

                    var newValue = grid[(int)newCoordinates.X, (int)newCoordinates.Y];
                    if (newValue != ' ')
                    {
                        direction = y == -1 ? Direction.Up : Direction.Down;
                        return newCoordinates;
                    }
                }

                return new Vector2(-1, -1);
            }

            void Advance()
            {
                switch (direction)
                {
                    case Direction.Up:
                        coordinates.Y = coordinates.Y - 1;
                        break;
                    case Direction.Down:
                        coordinates.Y = coordinates.Y + 1;
                        break;
                    case Direction.Right:
                        coordinates.X = coordinates.X + 1;
                        break;
                    case Direction.Left:
                        coordinates.X = coordinates.X - 1;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
