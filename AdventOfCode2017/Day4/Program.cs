﻿using System;
using System.Linq;
using SharedLibrary;

namespace Day4
{
    class Program
    {
        static void Main(string[] args)
        {
            var lines = FileHelper.GetLines("Day4").ToList();

            var total = 0;
            foreach (var line in lines)
            {
                var phrases = line.Split(null).ToList();
                var distinct = phrases.Distinct().ToList();
                if (phrases.SequenceEqual(distinct)) total++;
            }

            Console.WriteLine($"Part1 solution is: {total}");

            total = 0;
            foreach (var line in lines)
            {
                var phrases = line.Split(null).ToList();
                var distinct = phrases.Distinct().ToList();
                if (!phrases.SequenceEqual(distinct))
                {
                    continue;
                }

                var phrasesValid = true;
                for (int i = 0; i < phrases.Count; i++)
                {
                    for (int j = i + 1; j < phrases.Count; j++)
                    {
                        var phrase1 = phrases[i];
                        var phrase2 = phrases[j];
                        if (phrase1.Length != phrase2.Length) continue;

                        var containsAllLetters = true;
                        foreach (var letter in phrase1)
                        {
                            if (phrase2.Contains(letter))
                            {
                                phrase2 = phrase2.Remove(phrase2.IndexOf(letter), 1);
                            }
                            else
                            {
                                containsAllLetters = false;
                                break;
                            }
                        }

                        phrasesValid = !containsAllLetters;

                        if (!phrasesValid) break;
                    }

                    if (!phrasesValid) break;
                }

                if (phrasesValid) total++;
            }

            Console.WriteLine($"Part2 solution is: {total}");
        }
    }
}
