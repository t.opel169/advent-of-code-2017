﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day10
{
    class Program
    {
        static void Main(string[] args)
        {
            Part1();
            Part2();
        }

        private static void Part2()
        {
            const string input = "192,69,168,160,78,1,166,28,0,83,198,2,254,255,41,12";
            const string extraLengths = "17, 31, 73, 47, 23";
            var numberList = Enumerable.Range(0, 256).ToList();

            var extraLengthsList = extraLengths.Split(',').Select(int.Parse).ToList();
            var lengths = new List<int>();

            foreach (var character in input)
            {
                lengths.Add(character);
            }

            lengths.AddRange(extraLengthsList);

            var currentPosition = 0;
            var skipSize = 0;

            for (var i = 0; i < 64; i++)
            {
                foreach (var length in lengths)
                {
                    for (var startIndex = 0; startIndex < length / 2; startIndex++)
                    {
                        var index = (startIndex + currentPosition) % numberList.Count;
                        var startPosition = index % numberList.Count;
                        var endPosition = Math.Abs(currentPosition + length - 1 - startIndex) % numberList.Count;
                        var first = numberList[startPosition];
                        numberList[startPosition] = numberList[endPosition];
                        numberList[endPosition] = first;
                    }

                    currentPosition += length + skipSize;
                    skipSize++;

                    //numberList.ForEach(n => Console.Write($"{n} "));
                    //Console.WriteLine();
                }

                //Console.WriteLine(numberList[0] * numberList[1]);
            }

            var denseHash = new List<int>();
            for (var i = 0; i < 16; i++)
            {
                denseHash.Add(numberList.GetRange(i * 16, 16).Aggregate((x, y) => x ^ y));
            }

            var hexList = denseHash.Select(n => n.ToString("x2"));

            Console.WriteLine($"Part2: {string.Join("", hexList)}");
        }

        private static void Part1()
        {
            const string input = "192,69,168,160,78,1,166,28,0,83,198,2,254,255,41,12";
            var numberList = Enumerable.Range(0, 256).ToList();

            var lengths = input.Split(',').Select(int.Parse);
            var currentPosition = 0;
            var skipSize = 0;

            foreach (var length in lengths)
            {
                for (var startIndex = 0; startIndex < length / 2; startIndex++)
                {
                    var index = (startIndex + currentPosition) % numberList.Count;
                    var startPosition = index % numberList.Count;
                    var endPosition = Math.Abs(currentPosition + length - 1 - startIndex) % numberList.Count;
                    var first = numberList[startPosition];
                    numberList[startPosition] = numberList[endPosition];
                    numberList[endPosition] = first;
                }

                currentPosition += length + skipSize;
                skipSize++;

                //numberList.ForEach(n => Console.Write($"{n} "));
                //Console.WriteLine();
            }

            Console.WriteLine($"Part1: {numberList[0] * numberList[1]}");
        }
    }
}
