﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Day15
{
    class Program
    {
        private static readonly ConcurrentQueue<int> RemaindersA = new ConcurrentQueue<int>();
        private static readonly ConcurrentQueue<int> RemaindersB = new ConcurrentQueue<int>();
        private const int Rounds = 40000000;

        private static async Task Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Part1();
            // Change rounds to 5000000 for part 2
            //Part2();

            var finalCount = await GetFinalCount();

            stopwatch.Stop();
            Console.WriteLine(finalCount);
            Console.WriteLine($"Time elapsed: {stopwatch.Elapsed}");

            Console.ReadKey();
        }

        private static void Part1()
        {
            ThreadPool.QueueUserWorkItem(s => GenerateRemainders(RemaindersA, 618, 16807, 0));
            ThreadPool.QueueUserWorkItem(s => GenerateRemainders(RemaindersB, 814, 48271, 0));
        }

        private static void Part2()
        {
            ThreadPool.QueueUserWorkItem(s => GenerateRemainders(RemaindersA, 618, 16807, 4));
            ThreadPool.QueueUserWorkItem(s => GenerateRemainders(RemaindersB, 814, 48271, 8));
        }

        private static Task<int> GetFinalCount()
        {
            var runTime = Rounds;
            var total = 0;

            var remainderA = 0;
            var remainderB = 0;

            while (runTime > 0)
            {
                if (remainderA == 0 && !RemaindersA.TryDequeue(out remainderA))
                {
                    continue;
                }

                if (remainderB == 0 && !RemaindersB.TryDequeue(out remainderB))
                {
                    continue;
                }

                if (EqualsAsBinary(remainderA, remainderB, 16)) total++;

                runTime--;
                remainderA = 0;
                remainderB = 0;
            }

            return Task.FromResult(total);
        }

        private static void GenerateRemainders(ConcurrentQueue<int> queue, int previousValue, int factor, int modulus)
        {
            var runTime = Rounds;            

            while (runTime > 0)
            {
                var remainder = (long)previousValue * factor % 2147483647;
                previousValue = (int)remainder;

                if (modulus != 0 && previousValue % modulus != 0) continue;

                queue.Enqueue(previousValue);
                runTime--;
            }
        }

        private static bool EqualsAsBinary(int number1, int number2, int binaryLength)
        {
            var i = 0;
            while (i < binaryLength)
            {
                if ((number1 & (1 << i)) != 0 != ((number2 & (1 << i)) != 0)) return false;

                i++;
            }

            return true;
        }

        private static string GetIntBinaryString(int n)
        {
            var b = new char[16];
            var pos = 15;
            var i = 0;

            while (i < 16)
            {
                if ((n & (1 << i)) != 0)
                {
                    b[pos] = '1';
                }
                else
                {
                    b[pos] = '0';
                }
                pos--;
                i++;
            }
            return new string(b);
        }
    }
}
