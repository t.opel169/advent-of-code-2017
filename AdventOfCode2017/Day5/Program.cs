﻿using SharedLibrary;
using System;
using System.Linq;

namespace Day5
{
    class Program
    {
        static void Main(string[] args)
        {
            var instructions = FileHelper.GetLines("Day5").Select(int.Parse).ToList();

            var numberOfSteps = 0;

            try
            {
                var nextIndex = 0;
                while (true)
                {
                    var previousIndex = nextIndex;
                    nextIndex = nextIndex + instructions[previousIndex];
                    instructions[previousIndex] = instructions[previousIndex] + 1;
                    numberOfSteps++;
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("Array escaped");
            }

            Console.WriteLine($"Part1 solution is: {numberOfSteps}");

            instructions = FileHelper.GetLines("Day5").Select(int.Parse).ToList();
            numberOfSteps = 0;

            try
            {
                var nextIndex = 0;
                while (true)
                {
                    var previousIndex = nextIndex;
                    nextIndex = nextIndex + instructions[previousIndex];
                    var instructionValue = instructions[previousIndex];
                    instructions[previousIndex] = instructionValue >= 3 ? instructionValue - 1 : instructionValue + 1;
                    numberOfSteps++;
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("Array escaped");
            }

            Console.WriteLine($"Part2 solution is: {numberOfSteps}");
        }
    }
}
