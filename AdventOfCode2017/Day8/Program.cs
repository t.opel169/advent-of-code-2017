﻿using SharedLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day8
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = FileHelper.GetLines("Day8").ToList();

            var highestValue = 0;
            var registers = new Dictionary<string, int>();
            foreach (var line in input)
            {
                var split = line.Split(null);

                var registerToModify = split[0];
                var operation = split[1];
                var operationValue = int.Parse(split[2]);
                var registerToCheck = split[4];
                var checkType = split[5];
                var checkValue = int.Parse(split[6]);

                registers.TryGetValue(registerToCheck, out var registerToCheckValue);

                var isConditionValid = false;
                switch (checkType)
                {
                    case ">":
                        isConditionValid = registerToCheckValue > checkValue;
                        break;
                    case "<":
                        isConditionValid = registerToCheckValue < checkValue;
                        break;
                    case ">=":
                        isConditionValid = registerToCheckValue >= checkValue;
                        break;
                    case "<=":
                        isConditionValid = registerToCheckValue <= checkValue;
                        break;
                    case "!=":
                        isConditionValid = registerToCheckValue != checkValue;
                        break;
                    case "==":
                        isConditionValid = registerToCheckValue == checkValue;
                        break;
                }

                if (isConditionValid)
                {
                    registers.TryGetValue(registerToModify, out var currentValue);
                    if (operation == "inc")
                    {
                        currentValue += operationValue;
                    }
                    else
                    {
                        currentValue -= operationValue;
                    }

                    registers[registerToModify] = currentValue;

                    highestValue = highestValue < currentValue ? currentValue : highestValue;
                }
            }

            Console.WriteLine($"Largest register value is: {registers.Values.Max()}");
            Console.WriteLine($"Largest ever register value is: {highestValue}");
        }
    }
}
