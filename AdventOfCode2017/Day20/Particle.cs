﻿using System;
using System.Numerics;

namespace Day20
{
    public class Particle
    {
        public int Index { get; set; }
        public Vector3 Position { get; set; }
        public Vector3 Velocity { get; set; }
        public Vector3 Acceleration { get; set; }

        public float DistanceToZero => Math.Abs(Position.X) + Math.Abs(Position.Y) + Math.Abs(Position.Z);

        public void Advance()
        {
            Velocity = new Vector3(Velocity.X + Acceleration.X, Velocity.Y + Acceleration.Y, Velocity.Z + Acceleration.Z);
            Position = new Vector3(Position.X + Velocity.X, Position.Y + Velocity.Y, Position.Z + Velocity.Z);
        }
    }
}