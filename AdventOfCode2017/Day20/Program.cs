﻿using SharedLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Day20
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = FileHelper.GetLines("Day20").ToList();
            var particles = new List<Particle>();

            for (var particleIndex = 0; particleIndex < input.Count; particleIndex++)
            {
                var line = input[particleIndex];
                var spaceSplit = line.Split(null);
                var particle = new Particle { Index = particleIndex };
                for (var index = 0; index < spaceSplit.Length; index++)
                {
                    var split = spaceSplit[index];
                    var numbersString = split.Split('<')[1].Split(',');
                    var vector = new Vector3(
                        int.Parse(numbersString[0]),
                        int.Parse(numbersString[1]),
                        int.Parse(numbersString[2].Replace(">", "")));

                    switch (index)
                    {
                        case 0:
                            particle.Position = vector;
                            break;
                        case 1:
                            particle.Velocity = vector;
                            break;
                        default:
                            particle.Acceleration = vector;
                            break;
                    }
                }

                particles.Add(particle);
            }

            const int counterMax = 1000;
            var counter = 0;
            Particle currentClosest = null;
            while (counter <= counterMax)
            {
                var newClosest = particles.Aggregate((curMin, x) => (curMin == null || (x.DistanceToZero) < curMin.DistanceToZero ? x : curMin));

                if (currentClosest == newClosest) counter++;
                else counter = 0;

                currentClosest = newClosest;
                particles.ForEach(p => p.Advance());

                // Comment this out for Part 1 result
                particles = particles.GroupBy(p => p.Position).Where(g => g.Count() == 1).Select(g => g.First()).ToList();
            }

            Console.WriteLine(currentClosest?.Index);
            Console.WriteLine(particles.Count);
            Console.ReadKey();
        }
    }
}
