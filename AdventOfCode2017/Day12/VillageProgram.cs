﻿using System.Collections.Generic;

namespace Day12
{
    public class VillageProgram
    {
        public int Id { get; set; }
        public List<VillageProgram> Connections { get; set; } = new List<VillageProgram>();

        public HashSet<int> AllConnections(HashSet<int> allConnections)
        {
            foreach (var connection in Connections)
            {
                if (allConnections.Contains(connection.Id)) continue;

                allConnections.Add(connection.Id);
                connection.AllConnections(allConnections);
            }

            return allConnections;
        }
    }
}