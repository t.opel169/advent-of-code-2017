﻿using SharedLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day12
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var input = FileHelper.GetLines("Day12").ToList();

            var programs = new Dictionary<int, VillageProgram>();

            foreach (var line in input)
            {
                var split = line.Split("<->");
                var programId = int.Parse(split[0]);
                VillageProgram program;
                if (programs.ContainsKey(programId))
                {
                    program = programs[programId];
                }
                else
                {
                    program = new VillageProgram { Id = programId };
                    programs.Add(programId, program);
                }

                var connectionIds = split[1].Split(',').Select(c => int.Parse(c.Trim())).ToList();

                foreach (var connectionId in connectionIds)
                {
                    VillageProgram connectedProgram;
                    if (programs.ContainsKey(connectionId))
                    {
                        connectedProgram = programs[connectionId];
                    }
                    else
                    {
                        connectedProgram = new VillageProgram { Id = connectionId };
                        programs.Add(connectionId, connectedProgram);
                    }

                    program.Connections.Add(connectedProgram);
                }
            }

            var allConnections = programs[0].AllConnections(new HashSet<int>());

            Console.WriteLine(allConnections.Count);

            var groupCount = 1;

            foreach (var program in programs.Values)
            {
                if (allConnections.Contains(program.Id)) continue;

                allConnections.UnionWith(program.AllConnections(new HashSet<int>()));
                groupCount++;
            }

            Console.WriteLine(groupCount);
        }
    }
}