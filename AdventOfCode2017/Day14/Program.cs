﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Day14
{
    class Program
    {
        static void Main(string[] args)
        {
            const string baseString = "ffayrhll";

            var grid = new int[128, 128];

            var totalSquares = 0;
            for (var i = 0; i < 128; i++)
            {
                var binaryString = GetBinaryString($"{baseString}-{i}");
                totalSquares += binaryString.Count(bs => int.Parse(bs.ToString()) == 1);

                for (var index = 0; index < binaryString.Length; index++)
                {
                    var character = binaryString[index];
                    grid[i, index] = int.Parse(character.ToString());
                }
            }

            Console.WriteLine(totalSquares);

            var regions = new Dictionary<Vector2, int>();
            var regionsCounter = 0;

            for (var y = 0; y < grid.GetLength(1); y++)
            {
                for (var x = 0; x < grid.GetLength(0); x++)
                {
                    var position = new Vector2(x, y);

                    if (grid[x, y] == 0 || regions.ContainsKey(position))
                    {
                        continue;
                    }

                    regions.Add(position, regionsCounter);
                    regionsCounter++;
                    CheckNeighbors(position, regions, grid, regionsCounter);
                }
            }

            Console.WriteLine(regions.Values.Distinct().Count());
        }

        private static void CheckNeighbors(Vector2 position, Dictionary<Vector2, int> regions, int[,] grid, int region)
        {
            for (var x = -1; x <= 1; x++)
            {
                var neighborX = position.X + x;
                var neighborY = position.Y;

                CheckNeighbor(neighborX, neighborY);
            }

            for (var y = -1; y <= 1; y++)
            {
                var neighborX = position.X;
                var neighborY = position.Y + y;

                CheckNeighbor(neighborX, neighborY);
            }

            void CheckNeighbor(float neighborX, float neighborY)
            {
                if (neighborX < 0 || neighborX >= grid.GetLength(0) || neighborY < 0 || neighborY >= grid.GetLength(1)) return;

                var realPosition = new Vector2(neighborX, neighborY);
                if (grid[(int)neighborX, (int)neighborY] == 1 && !regions.ContainsKey(realPosition))
                {
                    regions.Add(realPosition, region);
                    CheckNeighbors(realPosition, regions, grid, region);
                }
            }
        }

        private static string GetBinaryString(string input)
        {
            const string extraLengths = "17, 31, 73, 47, 23";
            var numberList = Enumerable.Range(0, 256).ToList();

            var extraLengthsList = extraLengths.Split(',').Select(int.Parse).ToList();
            var lengths = new List<int>();

            foreach (var character in input)
            {
                lengths.Add(character);
            }

            lengths.AddRange(extraLengthsList);

            var currentPosition = 0;
            var skipSize = 0;

            for (var i = 0; i < 64; i++)
            {
                foreach (var length in lengths)
                {
                    for (var startIndex = 0; startIndex < length / 2; startIndex++)
                    {
                        var index = (startIndex + currentPosition) % numberList.Count;
                        var startPosition = index % numberList.Count;
                        var endPosition = Math.Abs(currentPosition + length - 1 - startIndex) % numberList.Count;
                        var first = numberList[startPosition];
                        numberList[startPosition] = numberList[endPosition];
                        numberList[endPosition] = first;
                    }

                    currentPosition += length + skipSize;
                    skipSize++;

                    //numberList.ForEach(n => Console.Write($"{n} "));
                    //Console.WriteLine();
                }

                //Console.WriteLine(numberList[0] * numberList[1]);
            }

            var denseHash = new List<int>();
            for (var i = 0; i < 16; i++)
            {
                denseHash.Add(numberList.GetRange(i * 16, 16).Aggregate((x, y) => x ^ y));
            }

            var hexList = denseHash.Select(n => n.ToString("x2"));
            var hexString = string.Join("", hexList);
            var binaryString = string.Join(string.Empty,
                hexString.Select(
                    c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')
                )
            );

            return binaryString;
        }
    }
}
