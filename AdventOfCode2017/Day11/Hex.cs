﻿using System;

namespace Day11
{
    public enum Direction
    {
        N,
        NE,
        SE,
        S,
        SW,
        NW
    }

    public struct Hex
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public void Move(Direction direction)
        {
            switch (direction)
            {
                case Direction.N:
                    X--;
                    Y++;
                    break;
                case Direction.NE:
                    Z--;
                    Y++;
                    break;
                case Direction.SE:
                    X++;
                    Z--;
                    break;
                case Direction.S:
                    X++;
                    Y--;
                    break;
                case Direction.SW:
                    Z++;
                    Y--;
                    break;
                case Direction.NW:
                    X--;
                    Z++;
                    break;
            }
        }
    }
}