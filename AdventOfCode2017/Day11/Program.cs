﻿using SharedLibrary;
using System;
using System.Linq;

namespace Day11
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = FileHelper.GetLines("Day11").ToList()[0];
            var directions = input.Split(',').Select(d => (Direction)Enum.Parse(typeof(Direction), d.ToUpper())).ToList();

            var hex = new Hex();
            var maxCoordinate = 0;
            foreach (var direction in directions)
            {
                hex.Move(direction);
                var currentMax = new[] { Math.Abs(hex.Y), Math.Abs(hex.X), Math.Abs(hex.Z) }.Max();
                if (maxCoordinate < currentMax) maxCoordinate = currentMax;
            }

            var endDistance = new[] { Math.Abs(hex.Y), Math.Abs(hex.X), Math.Abs(hex.Z) }.Max();

            Console.WriteLine($"Part 1: {endDistance}");
            Console.WriteLine($"Part 2: {maxCoordinate}");
        }
    }
}
