﻿using SharedLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day7
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = FileHelper.GetLines("Day7").ToList();

            var discs = new Dictionary<string, Disc>();
            var allChildren = new HashSet<string>();
            foreach (var line in input)
            {
                var spaceSplit = line.Split(null);
                var name = spaceSplit[0];
                var weight = int.Parse(spaceSplit[1].Replace("(", "").Replace(")", ""));

                Disc disc;
                if (discs.ContainsKey(name))
                {
                    disc = discs[name];
                    disc.Weight = weight;
                }
                else
                {
                    disc = new Disc(name, weight);
                    discs.Add(name, disc);
                }

                var arrowSplit = line.Split('>');
                if (arrowSplit.Length > 1)
                {
                    var childDiscsSplit = arrowSplit[1].Split(',').Select(d => d.Trim());
                    foreach (var discName in childDiscsSplit)
                    {
                        var childDisc = discs.ContainsKey(discName) ? discs[discName] : new Disc { Name = discName };
                        disc.ChildDiscs.Add(childDisc);
                        if (!discs.ContainsKey(discName)) discs.Add(discName, childDisc);
                        allChildren.Add(discName);
                    }
                }
            }

            var root = discs.Values.First(d => !allChildren.Contains(d.Name));

            Console.WriteLine($"Part1 solution is: {root.Name}");

            Console.WriteLine($"Part2 solution is: {GetBalancedWeight(root)}");

            Console.ReadKey();
        }

        private static int GetBalancedWeight(Disc disc)
        {
            var weights = new Dictionary<int, int>();
            foreach (var childDisc in disc.ChildDiscs)
            {
                var subTowerWeight = childDisc.DiscSubTowerWeight();
                if (weights.ContainsKey(subTowerWeight))
                {
                    weights[subTowerWeight] = weights[subTowerWeight] + 1;
                }
                else
                {
                    weights.Add(subTowerWeight, 1);
                }
            }

            if (weights.Count > 1)
            {
                var wrongSubTowerWeight = weights.First(w => w.Value == 1).Key;
                var wrongDisc = disc.ChildDiscs.First(d => d.DiscSubTowerWeight() == wrongSubTowerWeight);
                if (wrongDisc.IsSubTreeBalanced())
                {
                    return disc.ChildDiscs.First(d => d.DiscSubTowerWeight() == wrongSubTowerWeight).Weight + (weights.First(w => w.Value != 1).Key - wrongSubTowerWeight);
                }
            }

            foreach (var childDisc in disc.ChildDiscs)
            {
                var weight = GetBalancedWeight(childDisc);
                if (weight == 0) continue;
                return weight;
            }

            return 0;
        }
    }
}
