﻿using System.Collections.Generic;
using System.Linq;

namespace Day7
{
    public class Disc
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public List<Disc> ChildDiscs { get; set; }

        public Disc()
        {
            ChildDiscs = new List<Disc>();
        }

        public Disc(string name, int weight) : this()
        {
            Name = name;
            Weight = weight;
        }

        public int DiscSubTowerWeight()
        {
            var weight = Weight;
            var subTowerWeight = ChildDiscs.Sum(d => d.DiscSubTowerWeight());
            return weight + subTowerWeight;
        }

        public bool IsSubTreeBalanced()
        {
            var weights = new Dictionary<int, int>();
            foreach (var childDisc in ChildDiscs)
            {
                var subTowerWeight = childDisc.DiscSubTowerWeight();
                if (weights.ContainsKey(subTowerWeight))
                {
                    weights[subTowerWeight] = weights[subTowerWeight] + 1;
                }
                else
                {
                    weights.Add(subTowerWeight, 1);
                }
            }

            return weights.Count == 1;
        }
    }
}