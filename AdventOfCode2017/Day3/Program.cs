﻿using System;

namespace Day3
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = 265149;
            var gridSize = (int)Math.Ceiling(Math.Sqrt(input)) + 4;

            var gridOffset = gridSize / 2;
            var grid = new int[gridSize, gridSize];

            var number = 1;
            var initialPosition = new Tuple<int, int>(gridOffset, gridOffset);
            var previousPosition = initialPosition;

            while (number <= input)
            {
                if (number == 1)
                {
                    grid[previousPosition.Item1, previousPosition.Item2] = number;
                }
                else
                {
                    var nextPosition = GetNextPosition(grid, previousPosition);
                    grid[nextPosition.Item1, nextPosition.Item2] = number;
                    previousPosition = nextPosition;
                }

                //PrintGrid(grid, gridSize);
                number++;
            }

            //PrintGrid(grid, gridSize);
            var pathLength = Math.Abs(previousPosition.Item1 - initialPosition.Item1) + Math.Abs(previousPosition.Item2 - initialPosition.Item2);
            Console.WriteLine($"Part 1 answer is: {pathLength}");

            grid = new int[gridSize, gridSize];

            input = 265149;
            number = 1;
            initialPosition = new Tuple<int, int>(gridOffset, gridOffset);
            previousPosition = initialPosition;

            var isFirstStep = true;
            while (number < input)
            {
                if (isFirstStep)
                {
                    grid[previousPosition.Item1, previousPosition.Item2] = number;
                    isFirstStep = false;
                }
                else
                {
                    var nextPosition = GetNextPosition(grid, previousPosition);
                    number = GetNodeValue(grid, nextPosition);
                    grid[nextPosition.Item1, nextPosition.Item2] = number;
                    previousPosition = nextPosition;
                }

                //PrintGrid(grid, gridSize);
            }

            //PrintGrid(grid, gridSize);
            Console.WriteLine($"Part 2 answer is: {number}");

            Console.ReadKey();
        }

        private static int GetNodeValue(int[,] grid, Tuple<int, int> previousPosition)
        {
            var value = 0;
            for (var x = -1; x <= 1; x++)
            {
                for (var y = -1; y <= 1; y++)
                {
                    value += grid[previousPosition.Item1 + x, previousPosition.Item2 + y];
                }
            }

            return value;
        }

        private static Tuple<int, int> GetNextPosition(int[,] grid, Tuple<int, int> previousPosition)
        {
            var leftOccupied = grid[previousPosition.Item1 - 1, previousPosition.Item2] != 0;
            var rightOccupied = grid[previousPosition.Item1 + 1, previousPosition.Item2] != 0;
            var topOccupied = grid[previousPosition.Item1, previousPosition.Item2 - 1] != 0;
            var bottomOccupied = grid[previousPosition.Item1, previousPosition.Item2 + 1] != 0;

            if (leftOccupied && !topOccupied)
            {
                // Move up
                return new Tuple<int, int>(previousPosition.Item1, previousPosition.Item2 - 1);
            }
            if (bottomOccupied && !leftOccupied)
            {
                // Move left
                return new Tuple<int, int>(previousPosition.Item1 - 1, previousPosition.Item2);
            }
            if (rightOccupied && !bottomOccupied)
            {
                // Move down
                return new Tuple<int, int>(previousPosition.Item1, previousPosition.Item2 + 1);
            }
            else
            {
                return new Tuple<int, int>(previousPosition.Item1 + 1, previousPosition.Item2);
            }
        }

        private static void PrintGrid(int[,] grid, int gridSize)
        {
            for (var i = 0; i < gridSize; i++)
            {
                for (var j = 0; j < gridSize; j++)
                {
                    Console.Write($"{grid[j, i]} ");
                }
                Console.WriteLine();
            }
        }
    }
}
