﻿using System;
using System.Linq;
using SharedLibrary;

namespace Day2
{
    class Program
    {
        static void Main(string[] args)
        {
            var lines = FileHelper.GetLines("Day2").ToList();

            var total = 0;

            foreach (var line in lines)
            {
                var numbers = line.Split(null).Select(int.Parse).ToList();
                var checksum = numbers.Max() - numbers.Min();
                total += checksum;
            }

            Console.WriteLine($"Part1 solution is: {total}");

            total = 0;
            foreach (var line in lines)
            {
                var numbers = line.Split(null).Select(int.Parse).ToList();
                var divisionResult = 0;
                for (int i = 0; i < numbers.Count; i++)
                {
                    for (int j = 0; j < numbers.Count; j++)
                    {
                        if (i == j) continue;

                        if (numbers[i] % numbers[j] == 0)
                        {
                            divisionResult = numbers[i] / numbers[j];
                            break;
                        }
                    }

                    if (divisionResult != 0) break;
                }

                total += divisionResult;
            }

            Console.WriteLine($"Part2 solution is: {total}");

            Console.ReadKey();
        }
    }
}
