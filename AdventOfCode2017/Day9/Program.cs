﻿using System;
using System.Linq;
using SharedLibrary;

namespace Day9
{
    class Program
    {
        private static int _totalScore;
        private static string _pattern;
        private static int _garbageCharacters;

        static void Main(string[] args)
        {
            _pattern = FileHelper.GetLines("Day9").ToList()[0];

            ParseGroup(1, 0);

            Console.WriteLine(_totalScore);
            Console.WriteLine(_garbageCharacters);
        }

        private static int ParseGroup(int index, int score)
        {
            var thisNodeScore = score + 1;
            var character = _pattern[index];
            while (character != '}')
            {
                switch (character)
                {
                    case '{':
                        index = ParseGroup(++index, thisNodeScore);
                        break;
                    case ',':
                        index++;
                        break;
                    case '<':
                        index = ParseGarbage(++index);
                        break;
                }
                character = _pattern[index];
            }

            _totalScore += thisNodeScore;
            return ++index;
        }

        private static int ParseGarbage(int index)
        {
            var character = _pattern[index];
            while (character != '>')
            {
                switch (character)
                {
                    case '!':
                        index += 2;
                        break;
                    default:
                        _garbageCharacters++;
                        index++;
                        break;
                }

                character = _pattern[index];
            }

            return ++index;
        }
    }
}
