﻿using System;
using System.Linq;
using SharedLibrary;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = FileHelper.GetLines("Day1").ToList()[0];

            var total = 0;
            for (int i = 0; i < input.Length; i++)
            {
                var nextIndex = i + 1;

                if (input[i] == input[nextIndex % input.Length])
                {
                    total += int.Parse(input[i].ToString());
                }
            }

            Console.WriteLine($"Part1 solution is: {total}");

            total = 0;
            for (int i = 0; i < input.Length; i++)
            {
                var nextIndex = i + input.Length / 2;

                if (input[i] == input[nextIndex % input.Length])
                {
                    total += int.Parse(input[i].ToString());
                }
            }

            Console.WriteLine($"Part2 solution is: {total}");

            Console.ReadKey();
        }
    }
}
