﻿using SharedLibrary;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Day18
{
    class Program
    {
        private static ConcurrentQueue<long> _queue1 = new ConcurrentQueue<long>();
        private static ConcurrentQueue<long> _queue2 = new ConcurrentQueue<long>();
        private static bool _program1Waits;
        private static bool _program2Waits;
        private static int _operationCounter;
        protected internal static List<Instruction> Instructions;

        static void Main(string[] args)
        {
            var input = FileHelper.GetLines("Day18").ToList();
            Instructions = new List<Instruction>();
            var registers1 = new List<Register>();
            var registers2 = new List<Register>();

            foreach (var line in input)
            {
                Instructions.Add(new Instruction(line));
                var registerName = line.Split(null)[1];
                if (!int.TryParse(registerName, out var number))
                {
                    registers1.Add(new Register { Name = registerName[0] });

                    var register2 = new Register { Name = registerName[0] };
                    if (register2.Name == 'p') register2.Value = 1;
                    registers2.Add(register2);
                }
            }

            ThreadPool.QueueUserWorkItem(s => LaunchRegisters(registers1, _queue1, _queue2, true));
            ThreadPool.QueueUserWorkItem(s => LaunchRegisters(registers2, _queue2, _queue1, false));

            while (true)
            {
                if (_program1Waits && _program2Waits)
                {
                    Console.WriteLine(_operationCounter);
                    Console.ReadKey();
                }
            }
            

            
        }

        private static void LaunchRegisters(List<Register> registers, ConcurrentQueue<long> myQueue, ConcurrentQueue<long> otherQueue, bool isProgram1)
        {
            var currentInstructionIndex = 0;

            while (true)
            {
                var currentInstruction = Instructions[currentInstructionIndex];
                var register = registers.FirstOrDefault(r => r.Name == currentInstruction.FirstValueString[0]);
                switch (currentInstruction.InstructionType)
                {
                    case InstructionType.Play:
                        otherQueue.Enqueue(currentInstruction.GetValues(registers).Item1);
                        if (!isProgram1) _operationCounter++;
                        break;
                    case InstructionType.Set:
                        register.Value = currentInstruction.GetValues(registers).Item2;
                        break;
                    case InstructionType.Increase:
                        register.Value += currentInstruction.GetValues(registers).Item2;
                        break;
                    case InstructionType.Multiply:
                        register.Value *= currentInstruction.GetValues(registers).Item2;
                        break;
                    case InstructionType.Modulate:
                        register.Value %= currentInstruction.GetValues(registers).Item2;
                        break;
                    case InstructionType.Recover:
                        var timer = new Stopwatch();
                        timer.Start();
                        while (myQueue.IsEmpty)
                        {
                            if (timer.Elapsed > TimeSpan.FromSeconds(5))
                            {
                                if (isProgram1) _program1Waits = true;
                                else _program2Waits = true;
                            }
                        }
                        timer.Stop();
                        if (isProgram1) _program1Waits = false;
                        else _program2Waits = false;

                        myQueue.TryDequeue(out var value);
                        register.Value = value;
                        break;
                }

                if (currentInstruction.InstructionType != InstructionType.Jump)
                {
                    currentInstructionIndex++;
                }
                else
                {
                    var values = currentInstruction.GetValues(registers);
                    if (values.Item1 > 0)
                    {
                        currentInstructionIndex += (int)values.Item2;
                    }
                    else
                    {
                        currentInstructionIndex++;
                    }
                }
            }
        }

        private void Part1()
        {
            var input = FileHelper.GetLines("Day18").ToList();
            var instructions = new List<Instruction>();
            var registers = new List<Register>();
            var currentInstructionIndex = 0;

            foreach (var line in input)
            {
                instructions.Add(new Instruction(line));
                var registerName = line.Split(null)[1];
                if (!int.TryParse(registerName, out var number))
                {
                    registers.Add(new Register { Name = registerName[0] });
                }
            }

            long recoveredFrequency = 0;

            while (recoveredFrequency == 0)
            {
                var currentInstruction = instructions[currentInstructionIndex];
                var register = registers.FirstOrDefault(r => r.Name == currentInstruction.FirstValueString[0]);
                switch (currentInstruction.InstructionType)
                {
                    case InstructionType.Play:
                        register?.PlaySound();
                        break;
                    case InstructionType.Set:
                        register.Value = currentInstruction.GetValues(registers).Item2;
                        break;
                    case InstructionType.Increase:
                        register.Value += currentInstruction.GetValues(registers).Item2;
                        break;
                    case InstructionType.Multiply:
                        register.Value *= currentInstruction.GetValues(registers).Item2;
                        break;
                    case InstructionType.Modulate:
                        register.Value %= currentInstruction.GetValues(registers).Item2;
                        break;
                    case InstructionType.Recover:
                        if (register.Value != 0) recoveredFrequency = register.LastPlayedSound;
                        break;
                }

                if (currentInstruction.InstructionType != InstructionType.Jump)
                {
                    currentInstructionIndex++;
                }
                else
                {
                    var values = currentInstruction.GetValues(registers);
                    if (values.Item1 > 0)
                    {
                        currentInstructionIndex += (int)values.Item2;
                    }
                    else
                    {
                        currentInstructionIndex++;
                    }
                }
            }

            Console.WriteLine(recoveredFrequency);
            Console.ReadKey();
        }
    }
}
