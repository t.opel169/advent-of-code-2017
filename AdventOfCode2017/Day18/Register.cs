﻿namespace Day18
{
    public class Register
    {
        public char Name { get; set; }
        public long Value { get; set; }
        public long LastPlayedSound { get; private set; }

        public void PlaySound()
        {
            LastPlayedSound = Value;
        }
    }
}