﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day18
{
    public enum InstructionType
    {
        Play,
        Set,
        Increase,
        Multiply,
        Modulate,
        Recover,
        Jump
    }

    public class Instruction
    {
        public string FirstValueString { get; set; }
        public int? FirstValueInteger { get; set; }
        public string SecondValueString { get; set; }
        public int? SecondValueInteger { get; set; }
        public InstructionType InstructionType { get; set; }

        public Instruction(string line)
        {
            var split = line.Split(null);
            switch (split[0])
            {
                case "snd":
                    InstructionType = InstructionType.Play;
                    break;
                case "set":
                    InstructionType = InstructionType.Set;
                    break;
                case "add":
                    InstructionType = InstructionType.Increase;
                    break;
                case "mul":
                    InstructionType = InstructionType.Multiply;
                    break;
                case "mod":
                    InstructionType = InstructionType.Modulate;
                    break;
                case "rcv":
                    InstructionType = InstructionType.Recover;
                    break;
                case "jgz":
                    InstructionType = InstructionType.Jump;
                    break;
            }

            FirstValueString = split[1];
            if (int.TryParse(split[1], out var firstIntValue)) FirstValueInteger = firstIntValue;

            if (split.Length > 2)
            {
                SecondValueString = split[2];
                if (int.TryParse(split[2], out var intValue)) SecondValueInteger = intValue;
            }
        }

        public Tuple<long, long> GetValues(List<Register> registers)
        {
            if (FirstValueInteger.HasValue && SecondValueInteger.HasValue)
            {
                return new Tuple<long, long>(FirstValueInteger.Value, SecondValueInteger.Value);
            }
            else if (FirstValueInteger.HasValue && !SecondValueInteger.HasValue)
            {
                return new Tuple<long, long>(FirstValueInteger.Value, registers.First(r => r.Name == SecondValueString[0]).Value);
            }
            else if (!FirstValueInteger.HasValue && SecondValueInteger.HasValue)
            {
                return new Tuple<long, long>(registers.First(r => r.Name == FirstValueString[0]).Value, SecondValueInteger.Value);
            }
            else if (string.IsNullOrWhiteSpace(SecondValueString))
            {
                return new Tuple<long, long>(registers.First(r => r.Name == FirstValueString[0]).Value, 0);
            }
            else
            {
                return new Tuple<long, long>(registers.First(r => r.Name == FirstValueString[0]).Value, registers.First(r => r.Name == SecondValueString[0]).Value);
            }
        }
    }
}