﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedLibrary;

namespace Day6
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = FileHelper.GetLines("Day6").ToList()[0];
            var memoryBanks = input.Split(null).Select(int.Parse).ToList();

            var steps = 0;
            var previousBanks = new HashSet<string>();

            
            while (!previousBanks.Contains(string.Join(',', memoryBanks)))
            {
                previousBanks.Add(string.Join(',', memoryBanks));
                var maxIndex = memoryBanks.IndexOf(memoryBanks.Max());
                var blocksCount = memoryBanks[maxIndex];
                memoryBanks[maxIndex] = 0;
                while (blocksCount > 0)
                {
                    maxIndex++;
                    memoryBanks[maxIndex % memoryBanks.Count] = memoryBanks[maxIndex % memoryBanks.Count] + 1;
                    blocksCount--;
                }

                steps++;
            }

            Console.WriteLine($"Part1 solution is: {steps}");


            input = FileHelper.GetLines("Day6").ToList()[0];
            memoryBanks = input.Split(null).Select(int.Parse).ToList();

            steps = 0;
            var previousBanksDictionary = new Dictionary<string, int>();

            while (!previousBanksDictionary.ContainsKey(string.Join(',', memoryBanks)))
            {
                previousBanksDictionary.Add(string.Join(',', memoryBanks), steps);
                var maxIndex = memoryBanks.IndexOf(memoryBanks.Max());
                var blocksCount = memoryBanks[maxIndex];
                memoryBanks[maxIndex] = 0;
                while (blocksCount > 0)
                {
                    maxIndex++;
                    memoryBanks[maxIndex % memoryBanks.Count] = memoryBanks[maxIndex % memoryBanks.Count] + 1;
                    blocksCount--;
                }

                steps++;
            }

            var firstOccurrenceStep = previousBanksDictionary[string.Join(',', memoryBanks)];

            Console.WriteLine($"Part2 solution is: {steps - firstOccurrenceStep}");
        }
    }
}
