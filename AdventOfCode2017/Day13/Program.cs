﻿using SharedLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day13
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = FileHelper.GetLines("Day13").ToList();

            var scanningAreas = new List<ScanningArea>();

            foreach (var line in input)
            {
                var split = line.Split(':');
                scanningAreas.Add(new ScanningArea { Depth = int.Parse(split[0]), Range = int.Parse(split[1]) });
            }

            var fireWall = new List<ScanningArea>();
            var maxDepth = scanningAreas.Max(f => f.Depth);
            for (var depth = 0; depth <= scanningAreas.Max(f => f.Depth); depth++)
            {
                var scanningArea = scanningAreas.FirstOrDefault(sa => sa.Depth == depth);
                fireWall.Add(scanningArea);
            }

            var delay = 0;
            var finished = false;
            var packets = new List<Packet>();

            while (!finished)
            {
                packets.Add(new Packet { Delay = delay, Position = -1 });

                for (var i = packets.Count - 1; i >= 0; i--)
                {
                    var packet = packets[i];
                    packet.Position++;
                    if (fireWall[packet.Position]?.ScannerIndex == 0)
                    {
                        packets.RemoveAt(i);
                    }
                    else if (packet.Position == maxDepth)
                    {
                        finished = true;
                    }
                }

                if (!finished)
                {
                    scanningAreas.ForEach(sa => sa.MoveScanner());
                    delay++;
                }
            }

            Console.WriteLine(delay - maxDepth);
            Console.ReadKey();
        }

        private static void Part1()
        {
            var input = FileHelper.GetLines("Day13").ToList();

            var scanningAreas = new List<ScanningArea>();

            foreach (var line in input)
            {
                var split = line.Split(':');
                scanningAreas.Add(new ScanningArea { Depth = int.Parse(split[0]), Range = int.Parse(split[1]) });
            }

            var fireWall = new List<ScanningArea>();
            var maxDepth = scanningAreas.Max(f => f.Depth);
            for (var depth = 0; depth <= scanningAreas.Max(f => f.Depth); depth++)
            {
                var scanningArea = scanningAreas.FirstOrDefault(sa => sa.Depth == depth);
                fireWall.Add(scanningArea);
            }

            var packetPosition = 0;
            var totalSeverity = 0;

            while (packetPosition <= maxDepth)
            {
                var scanningArea = fireWall[packetPosition];
                var isCaught = scanningArea?.ScannerIndex == 0;

                if (isCaught)
                {
                    totalSeverity += scanningArea.Depth * scanningArea.Range;
                }

                scanningAreas.ForEach(sa => sa.MoveScanner());
                packetPosition++;
            }

            Console.WriteLine(totalSeverity);
        }
    }
}
