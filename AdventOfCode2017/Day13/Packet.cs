﻿namespace Day13
{
    public class Packet
    {
        public int Position { get; set; }
        public int Delay { get; set; }
    }
}