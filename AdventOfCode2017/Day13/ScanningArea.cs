﻿namespace Day13
{
    public class ScanningArea
    {
        public int Depth { get; set; }
        public int Range { get; set; }
        public int ScannerIndex { get; set; }
        private bool GoingDown { get; set; } = true;

        public void MoveScanner()
        {
            if (GoingDown)
            {
                if (ScannerIndex < Range - 1) ScannerIndex++;
                else
                {
                    GoingDown = false;
                    ScannerIndex--;
                }
            }
            else
            {
                if (ScannerIndex > 0) ScannerIndex--;
                else
                {
                    GoingDown = true;
                    ScannerIndex++;
                }
            }
        }
    }
}