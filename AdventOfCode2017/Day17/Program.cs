﻿using System;
using System.Collections.Generic;

namespace Day17
{
    class Program
    {
        static void Main(string[] args)
        {
            JustPart2();
            Console.ReadKey();
        }

        private static void JustPart2()
        {
            const int steps = 303;
            var result = 0;
            var index = 0;

            for (int i = 1; i <= 50000001; i++)
            {
                index = (index + steps) % i + 1;
                if (index == 1)
                {
                    result = i;
                }
            }

            Console.WriteLine(result);
        }

        private static void LinkedListSolution()
        {
            const int steps = 303;
            var values = new LinkedList<int>();
            var currentValue = 0;

            values.AddFirst(++currentValue);
            var currentNode = values.First;
            var zeroNode = currentNode;

            while (currentValue <= 50000000)
            {
                for (var i = 0; i < steps; i++)
                {
                    currentNode = currentNode.Next ?? values.First;
                }

                currentNode = values.AddAfter(currentNode, currentValue);

                currentValue++;
            }

            Console.WriteLine(zeroNode.Next?.Value);
        }
    }
}
