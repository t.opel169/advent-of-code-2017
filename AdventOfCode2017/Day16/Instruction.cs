﻿namespace Day16
{
    public enum InstructionType
    {
        Spin,
        IndexSwap,
        ProgramSwap
    }

    public abstract class Instruction
    {
        public InstructionType InstructionType { get; set; }
    }

    public class SpinInstruction : Instruction
    {
        public int SpinSize { get; }

        public SpinInstruction(int spinSize)
        {
            SpinSize = spinSize;
            InstructionType = InstructionType.Spin;
        }
    }

    public class IndexSwapInstruction : Instruction
    {
        public int Index1 { get; }
        public int Index2 { get; }

        public IndexSwapInstruction(int index1, int index2)
        {
            Index1 = index1;
            Index2 = index2;
            InstructionType = InstructionType.IndexSwap;
        }
    }

    public class ProgramSwapInstruction : Instruction
    {
        public string Program1 { get; }
        public string Program2 { get; }

        public ProgramSwapInstruction(string program1, string program2)
        {
            Program1 = program1;
            Program2 = program2;
            InstructionType = InstructionType.ProgramSwap;
        }
    }
}