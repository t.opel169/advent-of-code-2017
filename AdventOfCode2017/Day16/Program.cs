﻿using SharedLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day16
{
    class Program
    {
        protected internal static List<char> Characters = new List<char>();
        private static List<char> _original = new List<char>();
        protected internal static int Positions = 16;

        static void Main(string[] args)
        {
            //Test();
            var times = 40;
            var initialCharacterIndex = (int)'a';
            for (int i = 0; i < Positions; i++)
            {
                Characters.Add((char)(initialCharacterIndex + i));
            }

            _original = new List<char>(Characters);
            var input = FileHelper.GetLines("Day16").ToList()[0];

            var instructions = ParseInstructions(input.Split(',').ToList());
            while (times > 0)
            {
                foreach (var instruction in instructions)
                {
                    DoInstruction(instruction);
                }

                //if (_original.SequenceEqual(Characters))
                //{

                //}

                times--;
            }

            Console.WriteLine(string.Join("", Characters));

            Console.ReadKey();
        }

        private static void Test()
        {
            var list = new List<int> { 3, 8, 9, 7, 6 };
            var shift = 4;
            var realShift = shift % 2 == 1 ? shift - 1 : shift;
            list.Reverse(0, realShift);
            list.Reverse(realShift, (list.Count - realShift));
            list.Reverse();
        }

        private static List<Instruction> ParseInstructions(IEnumerable<string> instructions)
        {
            var instructionsList = new List<Instruction>();
            foreach (var instruction in instructions)
            {
                if (instruction.Contains('/'))
                {
                    var firstLetter = instruction[0];
                    var split = instruction.Substring(1).Split('/');

                    if (firstLetter == 'x')
                    {
                        var firstIndex = int.Parse(split[0]);
                        var secondIndex = int.Parse(split[1]);
                        instructionsList.Add(new IndexSwapInstruction(firstIndex, secondIndex));
                    }
                    else
                    {
                        var first = split[0];
                        var second = split[1];
                        instructionsList.Add(new ProgramSwapInstruction(first, second));
                    }
                }
                else
                {
                    var spinSize = int.Parse(instruction.Substring(1));
                    instructionsList.Add(new SpinInstruction(spinSize));
                }
            }

            return instructionsList;
        }

        private static void DoInstruction(Instruction instruction)
        {
            switch (instruction.InstructionType)
            {
                case InstructionType.Spin:
                    var spinInstruction = (SpinInstruction)instruction;

                    var newCharacters = new List<char>(Characters);
                    for (int i = 0; i < newCharacters.Count; i++)
                    {
                        newCharacters[(i + spinInstruction.SpinSize) % newCharacters.Count] = Characters[i];
                    }

                    //var size = spinInstruction.SpinSize % 2 == 1 ? spinInstruction.SpinSize - 1 : spinInstruction.SpinSize;
                    //Characters.Reverse(0, size);
                    //Characters.Reverse(size, (Positions - size));
                    //Characters.Reverse();

                    //if (!Characters.SequenceEqual(newCharacters))
                    //{

                    //}
                    Characters = newCharacters;

                    break;
                case InstructionType.IndexSwap:
                    var swapInstruction = (IndexSwapInstruction)instruction;
                    var firstValue = Characters[swapInstruction.Index1];
                    Characters[swapInstruction.Index1] = Characters[swapInstruction.Index2];
                    Characters[swapInstruction.Index2] = firstValue;
                    break;
                case InstructionType.ProgramSwap:
                    var programSwapInstruction = (ProgramSwapInstruction)instruction;
                    var firstIndex = Characters.IndexOf(programSwapInstruction.Program1[0]);
                    var secondIndex = Characters.IndexOf(programSwapInstruction.Program2[0]);
                    var value = Characters[firstIndex];
                    Characters[firstIndex] = Characters[secondIndex];
                    Characters[secondIndex] = value;
                    break;
            }
        }
    }
}
